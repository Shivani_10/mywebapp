import React from "react";
import NavBar from "../Navbar";
import { HeroContainer, HeroContent, HeroItems, HeroH1, HeroP, HeroBtn } from "../Hero/HeroEl.js";
export const HeroComp = () => {
  return (
    <>
      <HeroContainer>
        <NavBar />
        <HeroContent>
          <HeroItems>
            <HeroH1>PLAY GAMES & WIN CASH</HeroH1>
            <HeroP>Handcrafted Real Money Shows Play-Live</HeroP>
            <HeroBtn></HeroBtn>
          </HeroItems>
        </HeroContent>
      </HeroContainer>
    </>
  );
};
export default HeroComp;
