import React from 'react'
import{Nav, NavIcon, Navlink } from'./NavbarE';
import * as FaIcons from 'react-icons/fa';

const Navbar = () => {
    return (
        <>
            <Nav>
                <Navlink to='/'> </Navlink>
                <NavIcon>
                    <FaIcons.FaBars/>
                    </NavIcon>
            </Nav>
        </>
    )
}

export default Navbar;
