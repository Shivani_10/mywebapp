import React from 'react';
import Navbar from './Components/Navbar';
import{BrowserRouter as Router} from 'react-router-dom';
import {GlobalStyle} from './globalStyle';
import HeroComp from './Components/Hero/HeroComp';
function App() {
  return (
    <Router>
      <GlobalStyle/>
      <Navbar/>
      <HeroComp/>
    </Router>
  );
};
export default App;
